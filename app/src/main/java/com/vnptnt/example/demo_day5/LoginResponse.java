package com.vnptnt.example.demo_day5;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponse {

    @Expose
    @SerializedName("DanhSachDonVi")
    private List<DanhSachDonVi> DanhSachDonVi;
    @Expose
    @SerializedName("UserInfo")
    private UserInfo UserInfo;
    @Expose
    @SerializedName("Token")
    private String Token;
    @Expose
    @SerializedName("ErrorDesc")
    private String ErrorDesc;
    @Expose
    @SerializedName("ErrorCode")
    private int ErrorCode;

    public List<LoginResponse.DanhSachDonVi> getDanhSachDonVi() {
        return DanhSachDonVi;
    }

    public void setDanhSachDonVi(List<LoginResponse.DanhSachDonVi> danhSachDonVi) {
        DanhSachDonVi = danhSachDonVi;
    }

    public LoginResponse.UserInfo getUserInfo() {
        return UserInfo;
    }

    public void setUserInfo(LoginResponse.UserInfo userInfo) {
        UserInfo = userInfo;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getErrorDesc() {
        return ErrorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        ErrorDesc = errorDesc;
    }

    public int getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(int errorCode) {
        ErrorCode = errorCode;
    }

    public class DanhSachDonVi {
        @Expose
        @SerializedName("DuongDanAnh")
        private String DuongDanAnh;
        @Expose
        @SerializedName("DiaChi")
        private String DiaChi;
        @Expose
        @SerializedName("Ten")
        private String Ten;
        @Expose
        @SerializedName("Id")
        private int Id;
    }

    public class UserInfo {
        @Expose
        @SerializedName("SoDienThoai")
        private String SoDienThoai;
        @Expose
        @SerializedName("Email")
        private String Email;
        @Expose
        @SerializedName("HoVaTen")
        private String HoVaTen;
        @Expose
        @SerializedName("TenDangNhap")
        private String TenDangNhap;
        @Expose
        @SerializedName("Id")
        private int Id;
    }
}
