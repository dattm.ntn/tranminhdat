package com.vnptnt.example.demo_day5;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.edUser)
    EditText edUser;
    @BindView(R.id.edPass)
    EditText edPass;
    @BindView(R.id.BtLogin)
    Button BtLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.BtLogin)
    public void onViewClicked() {
        getData();
    }
    private void getData() {

        Account account = new Account("testapp1","Abc@1234","14843461661464","IOS","dfh;aguoafgmagp'gaafa64dgfgsgs6fg44s3g2sf9g");
        //account.setTenDangNhap = R.id.edUser;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIManager.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIManager service = retrofit.create(APIManager.class);
        service.Login(account).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.body() == null)
                {
                    return;
                }

                LoginResponse item = response.body();
                Log.d("123", response.body().toString());
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                //intent.putExtra("USB", "Kington");
                //String userName = edUserName.getText().toString();
                //intent.putExtra("USERNAME", userName);
                //intent.putExtra("AGE", "30");
                startActivity(intent);


            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.d("MainActivity", "onFailure" + t);
            }
        });

    }
}