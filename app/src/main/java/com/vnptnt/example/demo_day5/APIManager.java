package com.vnptnt.example.demo_day5;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface APIManager {
    String SERVER_URL = "http://bms.mis.vn/";

    @POST("api/MobileNhanVien/DangNhap")
    Call<LoginResponse> Login(@Body Account account);
}
